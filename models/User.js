var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */
var User = new keystone.List('User');

var MediaStorage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
    schema: {
        originalname: true,
    },
	fs: {
		path: keystone.expandPath('./public/images'),
		publicPath: '/public/images',
	},
});

User.add({
	name    : { type: Types.Name, required: true, index: true },
	age     : { type: Types.Date, label: "Date de naissance" },
	imgp    : { type: Types.File, storage: MediaStorage, label: "Photo de profil" },
	desc    : { type: Types.Html, wysiwyg: true, label: "Description" },
	email   : { type: Types.Email, initial: true, required: true, unique: true, index: true },
	password: { type: Types.Password, initial: true, required: true },
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true },
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});


/**
 * Registration
 */
User.defaultColumns = 'name, email, isAdmin';
User.register();
