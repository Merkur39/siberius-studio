var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Homepage Model
 * ==========
 */
var Homepage = new keystone.List('homepage');

var MediaStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    schema: {
        originalname: true,
      },
    fs: {
        path: keystone.expandPath('./public/images'),
        publicPath: '/public/images',
    },
});

Homepage.add({
	bann: { type: Types.File, storage: MediaStorage, label: "Bannière page Accueil" },
	socialsfb: {
        img: { type: Types.File, storage: MediaStorage, label: "Logo Facebook" },
        url: { type: String, label: "Url Facebook", default: "#" }
    },socialstwt: {
        img: { type: Types.File, storage: MediaStorage, label: "Logo Twitter" },
        url: { type: String, label: "Url Twitter", default: "#" }
    },socialsyt: {
        img: { type: Types.File, storage: MediaStorage, label: "Logo Youtube" },
        url: { type: String, label: "Url Youtube", default: "#" }
    },
});

/**
 * Registration
 * ============
 */
Homepage.defaultColumns = '';
Homepage.register();