var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Game Model
 * ==========
 */
var Game = new keystone.List('Game', {
    autokey: { from: 'name', path: 'slug', unique: true },
});

 var MediaStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    schema: {
        originalname: true,
    },
    fs: {
        path: keystone.expandPath('./public/images'),
        publicPath: '/public/images',
    },
});

Game.add({
	author : { type: Types.Relationship, ref: 'User', label: "Auteur" },
	name   : { type: String, required: true, unique: true, label: "Nom du jeu" },
    bann0  : { type: Types.File, storage: MediaStorage, label: "Bannière page Accueil" },
    bann1  : { type: Types.File, storage: MediaStorage, label: "Bannière page game" },
    colorbg: { type: Types.Text, label: "Code couleur BG", default: "#0F131B" },
    colorText: { type: Types.Text, label: "Code couleur Texte (Gras)", default: "#EEC32F" },
    video  : { type: Types.Text, label: "Vidéo YT", required: true, default: "utVlkCbd7OM" },
    steam: {
        img: { type: Types.File, storage: MediaStorage, label: "Logo Steam" },
        url: { type: String, label: "Url Steam", default: "#" }
    },
    desc   : { type: Types.Html, wysiwyg: true, label: "Description" },
    contentText: {
        text1: { type: Types.Html, wysiwyg: true, label: "1er Paragraphe" },
        text2: { type: Types.Html, wysiwyg: true, label: "2eme Paragraphe" }
    },
    contentGif: {
        gif1: { type: Types.File, storage: MediaStorage },
        gif2: { type: Types.File, storage: MediaStorage }
    },
	created: { type: Types.Datetime, default: Date.now, label: "Date de création" }
});

/**
 * Registration
 * ============
 */
Game.defaultColumns = 'name';
Game.register();
