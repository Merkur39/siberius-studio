var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * News Model
 * ==========
 */
var News = new keystone.List('News');

News.add({
	name   : { type: String, required: true, unique: true },
	describ: { type: Types.Html, wysiwyg: true },
    created: { type: Types.Datetime, default: Date.now },
    game   : { type: Types.Relationship, ref: 'Game' }
});

/**
 * Registration
 * ============
 */
News.defaultColumns = 'name|75%, game|25%';
News.register();
