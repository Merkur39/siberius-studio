/**
 * This file contains the common middleware used by your routes.
 *
 * Extend or replace these functions as your application requires.
 *
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */
var _        = require('lodash'),
	request  = require('request-promise'),
	keystone = require('keystone');

//#region getStreamApi
var urlTwitch = "https://api.twitch.tv/kraken/streams/"+process.env.TWITCH_CLIENT_NAME+"?client_id="+process.env.TWITCH_CLIENT_ID;
var options   = { method: 'GET', uri: urlTwitch, json: true };

exports.getStream = function(req, res, next) {
	request(options)
	.then(function (response) {
		if(response.stream === null) {
			res.locals.isActiveStream   = 0;
			res.locals.twitchClientName = process.env.TWITCH_CLIENT_NAME;
			next();
		 }else {
			res.locals.isActiveStream   = 1;
			res.locals.nbViewers        = response.stream.viewers;
			res.locals.titleChannel     = response.stream.channel.status;
			res.locals.twitchClientName = process.env.TWITCH_CLIENT_NAME;
			next();
		 };
	})
	.catch(function (err) { console.log('error:', err); next(err) });
};
//#endregion getStreamApi

//#region getNavLinks
exports.getNavLinks = function(req, res, next) {
	keystone.list('Game').model.find()
		.sort('created')
		.exec(function (err, resGames) {
			if(err) next(err);
			res.locals.navLinks = resGames;
			next();
		});
}
//#endregion getNavLinks

//#region getHomepageLinks
// BG & Links socials
exports.getHomepageLinks = function(req, res, next) {
	keystone.list('homepage').model.findOne()
		.exec(function(err, resHome){
			if(err) next(err);
			res.locals.homepage = resHome;
			next();
		});
}
//#endregion getHomepageLinks


exports.initLocals = function (req, res, next) {
	res.locals.user = req.user;
	next();
};

//Inits the error handler functions into `res`
exports.initErrorHandlers = function(req, res, next) {
    res.err = function(err, title, message) {
        res.status(500).render('errors/500', {
            err: err,
            errorTitle: title,
            errorMsg:   message
        });
    }
    res.notfound = function(title, message) {
        res.status(404).render('errors/404', {
            errorTitle: title,
            errorMsg:   message
        });
    }
    next();
    
};

//Fetches and clears the flashMessages before a view is rendered
exports.flashMessages = function (req, res, next) {
	var flashMessages = {
		info:    req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error:   req.flash('error'),
	};
	res.locals.messages = _.some(flashMessages, function (msgs) { return msgs.length; }) ? flashMessages : false;
	next();
};

//Prevents people from accessing protected pages when they're not signed in
exports.requireUser = function (req, res, next) {
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/');
	} else {
		next();
	}
};
