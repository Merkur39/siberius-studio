var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view   = new keystone.View(req, res),
	    locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.

	// locals.section = 'news';

	locals.rgbText       = {} // HEX to RGB Text
	locals.rgbBg         = {} // HEX to RGB Bg
	locals.rgbBgDarkened = {} // HEX to RGB Darkened
	locals.rgbBgWhitened = {} // HEX to RGB Whitened
	locals.game          = [];
	locals.news          = [];

	locals.section = req.params.game;

	//#region Functions hexToRgb
	var inverter = 20; // Ajout et retrait du rgb
	function hexToRgb(hex) {
		// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b;
		});
	
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	};
	function hexToRgbDarkened(hex) {
		// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b;
		});
	
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16) - inverter,
			g: parseInt(result[2], 16) - inverter,
			b: parseInt(result[3], 16) - inverter
		} : null;
	};
	function hexToRgbWhitened(hex) {
		// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b;
		});
	
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16) + inverter,
			g: parseInt(result[2], 16) + inverter,
			b: parseInt(result[3], 16) + inverter
		} : null;
	};
	//#endregion

    view.on('init', function(next){
        keystone.list('Game').model.findOne({ slug: req.params.game })
            .exec(function(err, result){
				if(err) next(err);
				if(result === null) res.redirect('/');
				locals.game          = result;
				locals.rgbText       = hexToRgb(result.colorText);
				locals.rgbBg         = hexToRgb(result.colorbg);
				locals.rgbBgDarkened = hexToRgbDarkened(result.colorbg);
				locals.rgbBgWhitened = hexToRgbWhitened(result.colorbg);
				next();
            });
	});

    view.on('init', function (next) {
		keystone.list('News').model.find()
			.where('game', locals.game)
			.sort('-created')
			.exec(function (err, resNews) {
				if(err) next(err);
				locals.news = resNews;
				next();
			});
	});


	// Render the view
	view.render('news');
};
