var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view   = new keystone.View(req, res),
	    locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	locals.users   = [];

	view.on('init', function (next) {
		keystone.list('User').model.find()
			.exec(function (err, resUser) {
				if(err) next(err);
				locals.users = resUser;
				next();
			});
	});

	// Render the view
	view.render('index');
};
